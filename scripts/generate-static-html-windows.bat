@echo off

docker run --rm -v .\:/spec --workdir /spec --entrypoint="/bin/sh" redocly/cli -c "/usr/local/bin/openapi build-docs openapi.yaml"

