@echo off

::keep this list of yaml up to date

docker run --rm -v .\:/spec --workdir /spec --entrypoint="/bin/sh" redocly/cli -c "/usr/local/bin/openapi join openAPI/paths/*.yaml -o openapi.yaml"

