# sevdesk-api-doku

sevdesk Api Doku mit redoc designed

# Folder structure

```
├── README.md
├── openAPI
│    ├── components
│    │    ├── banking.yaml
│    │    ├── contact.yaml
│    │    ├── contactCustomFieldSetting.yaml
│    │    ...
│    │
│    ├── img
│    └── paths
│        ├── _generalInformation.yaml
│        ├── banking.yaml
│        ├── contact.yaml
│        ...
│
└── openapi.yaml (generated)
```


### Technology
- [Redocly](https://redocly.com/)
- [YAML](https://yaml.org/)

## Setup

Nothing should be necessary for setting up the documentation or working with it. The script you can find below and in the scripts folder will use Docker, so that must be installed.

## Testing

It's possible to test:
- Open your browser and go to [http://localhost:63342/api_dokumentation/](http://localhost:63342/api_dokumentation/)

### Update local openapi.yaml:

Requirement: npm

1. ```npm i -g @redocly/cli@latest```
2. ```redocly join ./openAPI/paths/*.yaml```

## Windows (no need to install npm & Co.):

```shell
# Update the openapi.yaml
.\scripts\generate-openapi-windows.bat

# Generate a static documentation
.\scripts\generate-static-html-windows.bat

# Lint the generated openapi.yaml file
.\scripts\lint-windows.
```

##  Linux (no need to install npm & Co. locally):

```shell
# join all yaml files into openapi.yaml
docker run --rm -v $PWD:/spec --workdir /spec --entrypoint="/bin/sh" redocly/cli -c "/usr/local/bin/openapi join openAPI/paths/*.yaml -o openapi.yaml"

# generate static documentation
docker run --rm -v $PWD:/spec redocly/cli build-docs openapi.yaml

# lint openapi.yaml
docker run --rm -v $PWD:/spec redocly/cli lint openapi.yaml
```

If you use these docker commands on Windows replace `$PWD:/spec` -> `.\:/spec` or use the .bat files in the scripts folder.

## More information
1. Where can I find the API documentation
The live API documentation can be found at the URL [sevdesk - API](https://api.sevdesk.de/)
If you push to the main branch the openapi.yaml will be 

2. What tool and plugin do I use best for editing
I used VSCode to edit the documentation and as a little help I installed the plugin "YAML". The plugin gives an outline of the file.
![alt text](./docs/images/yaml.png "YAML Plugin")

3. How do I publish the documentation
Anyone can create a branch, but only the maintainers and owners can do the merge.

4. What is the best way to create a new endpoint
You can take the template from the gitlab project and copy out the endpoint there and then adjust it. There is also some more information in the template that should help you.
![alt text](./docs/images/template.png "Template")
