const redocSource = 'openapi.yaml';
const redocContainer = document.getElementById('redoc-container');
const redocOptions ={
    theme: {
        logo: {
            maxHeight: '73px'
        },
        typography: {
            links: {
                color: '#4a89dc',
                visited: '#4a89dc'
            }
        }
    }
};

const cleanVersioningHashtags = () => {
    const elements = document.querySelectorAll(".http-verb");

    const TEXT_TO_SEARCH = '#';
    elements.forEach((element) => {
        const path = element.nextSibling;
        const indexOfHash = path.innerText.indexOf(TEXT_TO_SEARCH);
        if (indexOfHash !== -1) {
            path.innerText = path.innerText.substring(0, indexOfHash);
            const subPanel = element.parentElement.nextElementSibling;
            subPanel.childNodes.forEach((children) => {
                const divButton = children.firstChild.nextSibling;
                const text = divButton.firstChild.lastChild;
                const indexOfHash = text.nodeValue.indexOf(TEXT_TO_SEARCH);
                if (indexOfHash !== -1) {
                    text.nodeValue = text.nodeValue.substring(0, indexOfHash);
                }
            });
        }
    });
};

const onFinishRedocRender = () => {
    const element = document.querySelector("div.api-content");

    const header = document.createElement("div");
    header.classList.add("api-header");
    const xhr = new XMLHttpRequest();
    xhr.open('GET', 'src/components/header.html', true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            header.innerHTML = xhr.responseText;
        }
    };
    xhr.send();
    element.insertBefore(header, element.firstChild);

    cleanVersioningHashtags();
}

Redoc.init(redocSource, redocOptions, redocContainer, onFinishRedocRender)
