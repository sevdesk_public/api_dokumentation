var popup;
window.addEventListener('load', function(){
    window.cookieconsent.initialise({
        //set revokeBtn if you don't want to see a tiny pullup bar overlapping your website
        //if revokeBtn is set, make sure to include a link to cookie settings in your footer
        //you can open your banner again with: popup.open();
        //revokeBtn: "<div class='cc-revoke'></div>",
        type: "opt-in",
        theme: "classic",
        "position": "bottom-left",
        "palette": {
            "popup": {
                "background": "#263238"
            },
            "button": {
                "background": "#4a89dc"
            }
        },
        "content": {
            "href": "https://sevdesk.com/security-data-protection/"
        },
        onInitialise: function(status) {
            // request gtag.js on page-load when the client already consented
            if(status === cookieconsent.status.allow) setCookies();
        },
        onStatusChange: function(status) {
            // resquest gtag cookies on a new consent
            if (this.hasConsented()) setCookies();
            else deleteCookies(this.options.cookie.name)
        },
        function (p) {
            popup = p;
        }
    })
});

const trackingId = 'G-38ZV6QHTJ4';
//it is absolutely crucial to define gtag in the global scope
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', trackingId);

function setCookies() {
    const s = document.createElement('script');
    s.type = "text/javascript"
    s.async = "true";
    s.src = "https://www.googletagmanager.com/gtag/js?id=" + trackingId;
    const x = document.getElementsByTagName('script')[0];
    x.parentNode.insertBefore(s, x);

};

function deleteCookies(cookieconsent_name) {
    const keep = [cookieconsent_name, "DYNSRV"];

    document.cookie.split(';').forEach(function(c) {
        c = c.split('=')[0].trim();
        if (!~keep.indexOf(c))
            document.cookie = c + '=;' + 'expires=Thu, 01 Jan 1970 00:00:00 UTC;path=/';
    });
};

function hashChanged(hash) {
    gtag('event', 'page_view', {
        page_title: window.document.title,
        page_location: window.location,
        page_path: hash,
        send_to: trackingId
    })
}

let storedHash = window.location.hash;
window.setInterval(function () {
    if (window.location.hash !== storedHash) {
        storedHash = window.location.hash;
        hashChanged(storedHash);
    }
}, 1000);
