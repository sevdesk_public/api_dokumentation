---
openapi: 3.0.0
info:
  title: sevdesk API
  description: Title
  version: 1.0.0
servers:
  - url: https://my.sevdesk.de/api/v1
    description: Our main application instance which most of our customers work with
  - url: http://sevdesk.local/api/v1
    description: sevdesk internal local instance
security:
  - api_key: []
paths:
  "/Report/invoicelist":
    get:
      summary: Export invoice list
      description: Export invoice list
      operationId: reportInvoice
      tags:
        - Report
      parameters:
        - name: download
          in: query
          schema:
            type: boolean
        - name: view
          in: query
          required: true
          schema:
            type: string
          example: all
        - name: sevQuery
          in: query
          required: true
          schema:
            type: object
            required:
              - modelName
              - objectName
            properties:
              limit:
                type: integer
                description: Limit export
                example: 1000
              modelName:
                description: Model name which is exported
                example: Invoice
              objectName:
                description: SevQuery object name
                example: SevQuery
              filter:
                type: object
                properties:
                  invoiceType:
                    type: array
                    description: |-
                      Type of invoices you want to export
                      1. RE - Rechnung
                      2. SR - Stornorechnung
                      3. TR - Teilrechnung
                      4. AR - Abschlagsrechnung
                      5. ER - Endrechnung
                      6. WKR - Wiederkehrende Rechnung
                      7. MA - Mahnung
                    items:
                      enum:
                        - Re
                        - SR
                        - TR
                        - AR
                        - ER
                        - WKR
                        - MA
                  startDate:
                    description: Start date of the invoice
                    type: string
                    format: date-time
                  endDate:
                    description: End date of the invoice
                    type: string
                    format: date-time
                  contact:
                    description: filters the invoices by contact
                    type: object
                    required:
                      - id
                      - objectName
                    properties:
                      id:
                        description: ID of the contact
                        type: integer
                      objectName:
                        description: Model name, which is 'Contact'
                        type: string
                        example: Contact
                  startAmount:
                    description: filters the invoices by amount
                    type: integer
                    example: 100
                  endAmount:
                    description: filters the invoices by amount
                    type: integer
                    example: 150
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                type: object
                properties:
                  objects:
                    type: object
                    properties:
                      filename:
                        type: string
                        example: invoice.pdf
                      mimetype:
                        type: string
                        example: application/pdf
                      base64Encoded:
                        type: boolean
                        example: true
                      content:
                        type: string
        '400':
          description: Bad request
        '401':
          description: Authentication required
        '409':
          description: Conflict
        '500':
          description: Server Error
  "/Report/orderlist":
    get:
      summary: Export order list
      description: Export order list
      operationId: reportOrder
      tags:
        - Report
      parameters:
        - name: download
          in: query
          schema:
            type: boolean
        - name: view
          in: query
          required: true
          schema:
            type: string
          example: all
        - name: sevQuery
          in: query
          required: true
          schema:
            type: object
            required:
              - modelName
              - objectName
            properties:
              limit:
                type: integer
                description: Limit export
                example: 1000
              modelName:
                description: Model name which is exported
                example: Order
              objectName:
                description: SevQuery object name
                example: SevQuery
              filter:
                type: object
                properties:
                  orderType:
                    type: string
                    description: |-
                      Type of orders you want to export
                      1. AN - Angebote
                      2. AB - Aufträge
                      3. LI - Lieferscheine
                    enum:
                     - AN
                     - AB
                     - LI
                  startDate:
                    description: Start date of the order
                    type: string
                    format: date-time
                  endDate:
                    description: End date of the order
                    type: string
                    format: date-time
                  contact:
                    description: filters the orders by contact
                    type: object
                    required:
                      - id
                      - objectName
                    properties:
                      id:
                        description: ID of the contact
                        type: integer
                      objectName:
                        description: Model name, which is 'Contact'
                        type: string
                        example: Contact
                  startAmount:
                    description: filters the orders by amount
                    type: integer
                    example: 100
                  endAmount:
                    description: filters the orders by amount
                    type: integer
                    example: 150
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                type: object
                properties:
                  objects:
                    type: object
                    properties:
                      filename:
                        type: string
                        example: order.pdf
                      mimetype:
                        type: string
                        example: application/pdf
                      base64Encoded:
                        type: boolean
                        example: true
                      content:
                        type: string
        '400':
          description: Bad request
        '401':
          description: Authentication required
        '409':
          description: Conflict
        '500':
          description: Server Error
  "/Report/contactlist":
    get:
      summary: Export contact list
      description: Export contact list
      operationId: reportContact
      tags:
        - Report
      parameters:
        - name: download
          in: query
          schema:
            type: boolean
        - name: sevQuery
          in: query
          required: true
          schema:
            type: object
            required:
              - modelName
              - objectName
            properties:
              limit:
                type: integer
                description: Limit export
                example: 1000
              modelName:
                description: Model name which is exported
                example: Contact
              objectName:
                description: SevQuery object name
                example: SevQuery
              filter:
                type: object
                properties:
                  zip:
                    description: filters the contacts by zip code
                    type: integer
                    example: 77656
                  city:
                    description: filters the contacts by city
                    type: string
                    example: Offenburg
                  country:
                    type: object
                    required:
                      - id
                      - objectName
                    properties:
                      id:
                        description: id of the country
                        type: integer
                        example: 1
                      objectName:
                        description: Model name, which is 'StaticCountry'
                        type: string
                        example: StaticCountry
                  depth:
                    description: export only organisations
                    type: boolean
                  onlyPeople:
                    description: export only people
                    type: boolean
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                type: object
                properties:
                  objects:
                    type: object
                    properties:
                      filename:
                        type: string
                        example: contact.pdf
                      mimetype:
                        type: string
                        example: application/pdf
                      base64Encoded:
                        type: boolean
                        example: true
                      content:
                        type: string
        '400':
          description: Bad request
        '401':
          description: Authentication required
        '409':
          description: Conflict
        '500':
          description: Server Error
  "/Report/voucherlist":
    get:
      summary: Export voucher list
      description: Export voucher list
      operationId: reportVoucher
      tags:
        -  Report
      parameters:
        - name: download
          in: query
          schema:
            type: boolean
        - name: sevQuery
          in: query
          required: true
          schema:
            type: object
            required:
              - modelName
              - objectName
            properties:
              limit:
                type: integer
                description: Limit export
                example: 1000
              modelName:
                description: Model name which is exported
                example: Voucher
              objectName:
                description: SevQuery object name
                example: SevQuery
              filter:
                type: object
                properties:
                  startDate:
                    description: Start date of the voucher
                    type: string
                    format: date-time
                  endDate:
                    description: End date of the voucher
                    type: string
                    format: date-time
                  startPayDate:
                    description: Start pay date of the voucher
                    type: string
                    format: date-time
                  endPayDate:
                    description: End pay date of the voucher
                    type: string
                    format: date-time
                  contact:
                    description: filters the vouchers by contact
                    type: object
                    required:
                      - id
                      - objectName
                    properties:
                      id:
                        description: ID of the contact
                        type: integer
                      objectName:
                        description: Model name, which is 'Contact'
                        type: string
                        example: Contact
                  startAmount:
                    description: filters the vouchers by amount
                    type: integer
                    example: 100
                  endAmount:
                    description: filters the vouchers by amount
                    type: integer
                    example: 150
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                type: object
                properties:
                  objects:
                    type: object
                    properties:
                      filename:
                        type: string
                        example: voucher.pdf
                      mimetype:
                        type: string
                        example: application/pdf
                      base64Encoded:
                        type: boolean
                        example: true
                      content:
                        type: string
        '400':
          description: Bad request
        '401':
          description: Authentication required
        '409':
          description: Conflict
        '500':
          description: Server Error
tags:
  - name: Report
    description: A set of operations to export data.